package com.colin.juc.flowcontrol.stampedLock;

import java.util.concurrent.locks.StampedLock;

/**
 * Created by zhaodong on 2021/5/16 16:0030
 * email: colinzhaodong@gmail.com
 * desc:
 *
 * @author zhaodong
 */
class Point {
    private int x, y;
    final StampedLock sl =
            new StampedLock();

    // 计算到原点的距离
    private int distanceFromOrigin() {
        // 乐观读
        long stamp =
                sl.tryOptimisticRead();
        // 读入局部变量，
        // 读的过程数据可能被修改
        x = 2;
        y = 3;
        int curX = x, curY = y;
        x = 3;
        // 判断执行读操作期间，
        // 是否存在写操作，如果存在，
        // 则 sl.validate 返回 false
        if (!sl.validate(stamp)) {
            // 升级为悲观读锁
            stamp = sl.readLock();
            try {
                curX = x;
                curY = y;
            } finally {
                // 释放悲观读锁
                sl.unlockRead(stamp);
            }
        }
        return (int) Math.sqrt(
                curX * curX + curY * curY);
    }

    public static void main(String[] args) {
        Point point = new Point();
        point.x = 1;
        point.y = 1;
        System.out.println(point.distanceFromOrigin());
    }
}
