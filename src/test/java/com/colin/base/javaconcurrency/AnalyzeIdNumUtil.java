package com.colin.base.javaconcurrency;


import com.colin.base.javaconcurrency.VO.AnalysisResultVO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Colin on 2020/6/18 10:28
 * email: colinzhaodong@gmail.com
 * desc: 分析身份证号工具类
 *
 * @author zhaod
 */
public class AnalyzeIdNumUtil {
	private static final int ID_LENGTH =18;

	public static String getYear(final String id)
	{
		int len=id.length();
		if(len< ID_LENGTH){
			return null;
		}
		return id.substring(6,10);
	}

	public static String getMonth(final String id)
	{
		int len=id.length();
		if(len< ID_LENGTH) {
			return null;
		}
		return id.substring(10,12);
	}

	public static Integer getSex(final String id){
		return Integer.parseInt(id.substring(16).substring(0, 1)) % 2 == 0 ? 0:1;
	}

	public static Integer getAge(final String id){
		String birthYear = AnalyzeIdNumUtil.getYear(id);
		String birthMonth = AnalyzeIdNumUtil.getMonth(id);
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String currentYear = format.format(date).substring(0, 4);
		String currentMonth = format.format(date).substring(5, 7);
		int age;
		// 当前月份大于用户出身的月份表示已过生
		assert birthMonth != null;
		assert birthYear != null;
		int tempAge = Integer.parseInt(currentYear) - Integer.parseInt(birthYear);
		if (Integer.parseInt(birthMonth) <= Integer.parseInt(currentMonth)) {
			age = tempAge + 1;
		} else {// 当前用户还没过生
			age = tempAge;
		}
		return age;
	}

	public static String getDay(final String id)
	{
		int len=id.length();
		if(len< ID_LENGTH){
			return null;
		}
		return id.substring(12,14);
	}

	public static Integer getConstellation(final String id)
	{
		int month=Integer.parseInt(Objects.requireNonNull(AnalyzeIdNumUtil.getMonth(id)));
		int day=Integer.parseInt(Objects.requireNonNull(AnalyzeIdNumUtil.getDay(id)));
		Integer value = 0;
		boolean aquariusFlag = ((month==1)   && (day >=20)) || ((month==2)  &&(day<=18));
		if(aquariusFlag) {
			value = ConstellationEnum.AQUARIUS.getCode();
		}
		boolean piscesFlag = ((month==2)   && (day >=19)) || ((month==3)  &&(day<=20));
		if(piscesFlag) {
			value = ConstellationEnum.PISCES.getCode();
		}
		boolean ariesFlag = ((month==3)   && (day >=21)) || ((month==4)  &&(day<=19));
		if(ariesFlag) {
			value = ConstellationEnum.ARIES.getCode();
		}
		boolean taurusFlag = ((month==4)   && (day >=20)) || ((month==5)  &&(day<=20));
		if(taurusFlag) {
			value = ConstellationEnum.TAURUS.getCode();
		}
		boolean geminiFlag = ((month==5)   && (day >=21)) || ((month==6)  &&(day<=21));
		if(geminiFlag) {
			value = ConstellationEnum.GEMINI.getCode();
		}
		boolean cancerFlag = ((month==6)   && (day >=22)) || ((month==7)  &&(day<=22));
		if(cancerFlag) {
			value = ConstellationEnum.CANCER.getCode();
		}
		boolean leoFlag = ((month==7)   && (day >=23)) || ((month==8)  &&(day<=22));
		if(leoFlag) {
			value = ConstellationEnum.LEO.getCode();
		}
		boolean virgoFlag = ((month==8)   && (day >=23)) || ((month==9)  &&(day<=22));
		if(virgoFlag) {
			value = ConstellationEnum.VIRGO.getCode();
		}
		boolean libraFlag = ((month==9)   && (day >=23)) || ((month==10) &&(day<=23));
		if(libraFlag) {
			value = ConstellationEnum.LIBRA.getCode();
		}
		boolean scorpioFlag = ((month==10)  && (day >=24)) || ((month==11) &&(day<=22));
		if(scorpioFlag) {
			value = ConstellationEnum.SCORPIO.getCode();
		}
		boolean sagittariusFlag = ((month==11)  && (day >=23)) || ((month==12) &&(day<=21));
		if(sagittariusFlag) {
			value = ConstellationEnum.SAGITTARIUS.getCode();
		}
		boolean capricornFlag = ((month==12)  && (day >=22)) || ((month==1)  &&(day<=19));
		if(capricornFlag) {
			value = ConstellationEnum.CAPRICORN.getCode();
		}

		return value;
	}

	public static String getZodiac(final String id)
	{
		int year=Integer.parseInt(Objects.requireNonNull(AnalyzeIdNumUtil.getYear(id)));
		String[] zodiac ={"鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"};
		int i=(year-4)%12;
		return zodiac[i];
	}

	public static AnalysisResultVO composeAnalysisResult(final String idNum){
		return AnalysisResultVO.create()
				.constellation(AnalyzeIdNumUtil.getConstellation(idNum))
				.age(AnalyzeIdNumUtil.getAge(idNum))
				.sex(AnalyzeIdNumUtil.getSex(idNum))
				.build();
	}

	public static void main(String[] args)
	{
		String idCard = "500381123323523216";
		String idCard2 = "320922199706296310";
		System.out.println("星座:" + ConstellationEnum.getName(AnalyzeIdNumUtil.getConstellation(idCard)));
		System.out.println("属相:" + AnalyzeIdNumUtil.getZodiac(idCard));
		System.out.println("年龄:" + AnalyzeIdNumUtil.getAge(idCard));
		System.out.println("性别:" + AnalyzeIdNumUtil.getSex(idCard));
	}
}
