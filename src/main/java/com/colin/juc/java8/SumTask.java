package com.colin.juc.java8;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * @author zhaodong 2021/5/29 14:3940
 * email: colinzhaodong@gmail.com
 * desc:
 */
class SumTask extends RecursiveTask<Long> {

    static final int THRESHOLD = 100;
    long[] array;
    int start;
    int end;

    SumTask(long[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        if (end - start <= THRESHOLD) {
            // 如果任务足够小,直接计算:
            long sum = 0;
            for (int i = start; i < end; i++) {
                sum += array[i];
            }
            System.out.println(String.format("compute %d~%d = %d", start, end, sum));
            return sum;
        }
        // 任务太大,划分:
        int middle = (end + start) / 2;
        System.out.println(String.format("split %d~%d ==> %d~%d, %d~%d", start, end, start, middle, middle, end));
        SumTask subtask1 = new SumTask(this.array, start, middle);
        subtask1.fork();
        SumTask subtask2 = new SumTask(this.array, middle, end);
        invokeAll(subtask1, subtask2);
        Long subresult1 = subtask1.join();
        Long subresult2 = subtask2.join();
        Long result = subresult1 + subresult2;
        System.out.println("result = " + subresult1 + " + " + subresult2 + " ==> " + result);
        return result;
    }

    public static void main(String[] args) {
        long[] array = new long[100000];
        fillRandom(array);
        ForkJoinPool fjp = new ForkJoinPool(4); // 最大并发数4
        ForkJoinTask<Long> sumTask = new SumTask(array,0,array.length);
        long startTime = System.currentTimeMillis();
        Long result = fjp.invoke(sumTask);
        long endTime = System.currentTimeMillis();
        System.out.println("Fork/join sum: " + result + " in " + (endTime - startTime) + " ms.");
        long startTime1 = System.currentTimeMillis();
        int result1 = singleThreadCompute(array);
        long endTime1 = System.currentTimeMillis();
        System.out.println("singleThreadCompute sum: " + result1 + " in " + (endTime1 - startTime1) + " ms.");
    }

    private static void fillRandom(long[] array) {
        Arrays.fill(array, 1);
    }

    private static int singleThreadCompute(long[] array){
        int result = 0;
        for (long l : array) {
            result += l;
        }
        return result;
    }
}
