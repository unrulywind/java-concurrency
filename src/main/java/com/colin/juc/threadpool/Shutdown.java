package com.colin.juc.threadpool;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Colin on 2020/4/4 14:37
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class Shutdown {
	public static void main(String[] args) throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 100; i++) {
			executorService.execute(new ShutDownTask());
		}
		Thread.sleep(1500);
		List<Runnable> taskList = executorService.shutdownNow();
		System.out.println(taskList.size());


//		boolean b = executorService.awaitTermination(3, TimeUnit.SECONDS);
//		System.out.println(b);



//		System.out.println(executorService.isShutdown());
//		executorService.shutdown();
//		System.out.println(executorService.isShutdown());
//		System.out.println(executorService.isTerminated());
//		executorService.execute(new ShutDownTask());
	}
}
class ShutDownTask implements Runnable {


	@Override
	public void run() {
		try {
			Thread.sleep(500);
			System.out.println(Thread.currentThread().getName());
		} catch (InterruptedException e) {
			System.out.println(Thread.currentThread().getName() + "被中断了");
		}
	}
}