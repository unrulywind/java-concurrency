package com.colin.base.javaconcurrency.createthreads.wrongways;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author: BlueMelancholy
 * 2019/12/16 16:28
 * @desc
 * @email zhaod@oceansoft.com.cn
 */
public class DemoTimerTask {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }, 500,1000 );
    }
}
