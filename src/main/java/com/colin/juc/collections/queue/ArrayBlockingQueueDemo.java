package com.colin.juc.collections.queue;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Colin on 2020/5/16 13:16
 * email: colinzhaodong@gmail.com
 * desc: 演示阻塞队列
 *
 * @author zhaod
 */
public class ArrayBlockingQueueDemo {

	public static void main(String[] args) {
		BlockingQueue<String> queue = new ArrayBlockingQueue<String>(3);

		Interviewer r1 = new Interviewer(queue);
		Consumer r2 = new Consumer(queue);
		new Thread(r1).start();
		new Thread(r2).start();
	}
}

class Interviewer implements Runnable {

	BlockingQueue<String> queue;

	public Interviewer(BlockingQueue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		System.out.println("10个候选人都来啦");
		for (int i = 0; i < 10; i++) {
			String candidate = "Candidate" + i;
			try {
				queue.put(candidate);
				System.out.println("安排好了" + candidate);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			queue.put("stop");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class Consumer implements Runnable {

	BlockingQueue<String> queue;

	public Consumer(BlockingQueue queue) {

		this.queue = queue;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String msg;
		try {
			while(!"stop".equals(msg = queue.take())){
				System.out.println(msg + "到了");
			}
			System.out.println("所有候选人都结束了");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
