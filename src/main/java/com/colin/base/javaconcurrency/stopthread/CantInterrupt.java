package com.colin.base.javaconcurrency.stopthread;

/**
 * @author: BlueMelancholy
 * 2019/12/17 14:27
 * @desc while里面放try/catch，会导致中断失效 sleep中断标记位被中断
 * @email zhaod@oceansoft.com.cn
 */
public class CantInterrupt {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = () -> {
          int num = 0;
          while(num <= 10000){
              if (num % 100 == 0 && !Thread.currentThread().isInterrupted()){
                  System.out.println(num + "是100的倍数");
              }
              num++;
              try {
                  Thread.sleep(10);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        Thread.sleep(5000);
        thread.interrupt();
    }
}
