package com.colin.juc.lock.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Colin on 2020/4/7 20:00
 * email: colinzhaodong@gmail.com
 * desc: lock需要手动释放锁
 *
 * @author zhaod
 */
public class MustUnlock {
	private static Lock lock = null;
	static {
		lock = new ReentrantLock();
	}

	public static void main(String[] args) {
		lock.lock();
		try {
			System.out.println(Thread.currentThread().getName());
		} finally {
			lock.unlock();
		}
	}
}
