package com.colin.juc.collections.Predecessor;

import java.util.Collections;
import java.util.Vector;

/**
 * Created by Colin on 2020/5/10 10:44
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class VectorDemo {

	public static void main(String[] args) {
		Vector<String> vector = new Vector<>();
		vector.add("test");
		System.out.println(vector.get(0));
	}
}
