package com.colin.base.javaconcurrency.createthreads;

/**
 * @author: BlueMelancholy
 * 2019/12/16 16:06
 * @desc
 * @email zhaod@oceansoft.com.cn
 */
public class BothRunnableThread {
    public static void main(String[] args) {
        new Thread(() -> System.out.println("来自Runnable")){
            @Override
            public void run() {
                System.out.println("来自Thread重新run方法");
            }
        }.start();
    }
}
