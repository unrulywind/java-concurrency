package com.colin.juc.designmode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhaodong 2021/6/4 19:1717
 * email: colinzhaodong@gmail.com
 * desc:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message<T> {
    T id;

    String msg;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
