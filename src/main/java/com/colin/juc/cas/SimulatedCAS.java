package com.colin.juc.cas;

/**
 * Created by Colin on 2020/4/21 20:01
 * email: colinzhaodong@gmail.com
 * desc: 模拟CAS操作
 *
 * @author zhaod
 */
public class SimulatedCAS {
	private volatile int value;

	public synchronized int compareAndSwap(int expectedValue,int newValue){
		int oldValue = value;
		if (expectedValue == oldValue){
			value = newValue;
		}
		return value;
	}
}
