package com.colin.base.javaconcurrency.threadobjectclasscommonmethods;

import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/3/8 19:26
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class JoinInterrupt {
	public static void main(String[] args) {
		Thread mainThread = Thread.currentThread();
		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					mainThread.interrupt();
					TimeUnit.MILLISECONDS.sleep(5000);
					System.out.println("Thread1 finished.");
				} catch (InterruptedException e) {
					System.out.println("子线程中断");
					e.printStackTrace();
				}
			}
		});
		thread1.start();
		System.out.println("等待子线程运行完毕");
		try {
			thread1.join();
		} catch (InterruptedException e) {
			System.out.println(Thread.currentThread().getName()+"主线程中断了");
			e.printStackTrace();
			thread1.interrupt();
		}
		System.out.println("子线程已运行完毕");
	}
}
