package com.colin.juc.flowcontrol.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Created by Colin on 2020/5/31 12:21
 * email: colinzhaodong@gmail.com
 * desc: 演示Semaphore信号量
 *
 * @author zhaod
 */
public class SemaphoreDemo {
	static Semaphore semaphore = new Semaphore(3,true);

	public static void main(String[] args) {
		ExecutorService service = Executors.newFixedThreadPool(50);
		for (int i = 0; i < 50; i++) {
			service.submit(new Task());
		}
		service.shutdown();
	}

	static class Task implements Runnable {

		@Override
		public void run() {
			try {
				semaphore.acquire(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName() + "拿到了许可证");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName() + "释放了许可证");
			semaphore.release(3);
		}
	}
}
