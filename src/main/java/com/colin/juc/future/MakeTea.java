package com.colin.juc.future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by zhaodong on 2021/5/19 17:0520
 * email: colinzhaodong@gmail.com
 * desc:
 *
 * @author zhaodong
 */
public class MakeTea {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 创建任务 T2 的 FutureTask
        T2Task t2Task = new T2Task();
        FutureTask<String> ft2
                = new FutureTask<>(t2Task);
        // 创建任务 T1 的 FutureTask
        T1Task t1Task = new T1Task(ft2);
        FutureTask<String> ft1
                = new FutureTask<>(t1Task);
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(ft1);
        executorService.submit(ft2);
        System.out.println(ft1.get());
    }
}

class T1Task implements Callable<String>{
    FutureTask<String> ft2;

    public T1Task(FutureTask<String> ft2) {
        this.ft2 = ft2;
    }

    @Override
    public String call() throws Exception {
        System.out.println("T1: 洗水壶...");
        TimeUnit.SECONDS.sleep(1);

        System.out.println("T1: 烧开水...");
        TimeUnit.SECONDS.sleep(15);
        // 获取 T2 线程的茶叶
        String tf = ft2.get();
        System.out.println("T1: 拿到茶叶:"+tf);

        System.out.println("T1: 泡茶...");
        return " 上茶:" + tf;
    }
}

class T2Task implements Callable<String>{
    @Override
    public String call() throws Exception {
        System.out.println("T2: 洗茶壶...");
        TimeUnit.SECONDS.sleep(1);

        System.out.println("T2: 洗茶杯...");
        TimeUnit.SECONDS.sleep(2);

        System.out.println("T2: 拿茶叶...");
        TimeUnit.SECONDS.sleep(1);
        return " 龙井 ";
    }
}