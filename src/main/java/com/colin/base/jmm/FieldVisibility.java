package com.colin.base.jmm;

import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/3/19 20:12
 * email: colinzhaodong@gmail.com
 * desc: 演示可见性带来的问题
 *
 * @author zhaod
 */
public class FieldVisibility {
	volatile int a = 1;
	volatile int b = 2;
	private void change() {
		a = 3;
		b = a;
	}


	private void print() {
		System.out.println("b=" + b + ";a=" + a);
	}
	public static void main(String[] args) {
		FieldVisibility test = new FieldVisibility();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						TimeUnit.MILLISECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					test.change();
				}
			}).start();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					test.print();
				}
			}).start();
	}
}
