package com.colin.base.deadlock;

import java.util.Random;

/**
 * Created by Colin on 2020/3/22 20:28
 * email: colinzhaodong@gmail.com
 * desc: 多人转账发生死锁
 *
 * @author zhaod
 */
public class MultiTransferMoney {
	private static final int NUM_ACCOUNTS = 500;
	private static final int NUM_MONEY = 1000;
	private static final int NUM_ITERATIONS = 10;
	private static final int NUM_THREADS = 10;

	public static void main(String[] args) {
		Random random = new Random();

		TransferMoney.Account[] accounts = new TransferMoney.Account[NUM_ACCOUNTS];
		for (int i = 0; i < accounts.length; i++) {
			accounts[i] = new TransferMoney.Account(NUM_MONEY);
		}
		class TransferThread extends Thread {

			@Override
			public void run() {
				for (int i = 0; i < NUM_ITERATIONS; i++) {
					int fromAcct = random.nextInt(NUM_ACCOUNTS);
					int toAcct = random.nextInt(NUM_ACCOUNTS);
					int amount = random.nextInt(NUM_MONEY);
					TransferMoney.transferMoney(accounts[fromAcct], accounts[toAcct], amount);
				}
				System.out.println("运行结束");
			}
		}
		for (int i = 0; i < NUM_THREADS; i++) {
			new TransferThread().start();
		}
	}
}
