package com.colin.base.javaconcurrency.uncaughtexception;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Colin on 2020/3/14 14:33
 * email: colinzhaodong@gmail.com
 * desc: 自己的MyUncaughtExceptionHanlder
 *
 * @author zhaod
 */
public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler{
	private String name;

	public MyUncaughtExceptionHandler(String name) {
		this.name = name;
	}
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		Logger logger = Logger.getAnonymousLogger();
		logger.log(Level.WARNING, "线程异常，终止啦" + t.getName());
		System.out.println(name + "捕获了异常" + t.getName() + "异常");
	}
}
