package com.colin.base.javaconcurrency.stopthread;

/**
 * @author: BlueMelancholy
 * 2019/12/17 14:52
 * @desc run无法抛出checked Exception,只能用try/catch
 * @email zhaod@oceansoft.com.cn
 */
public class RunThrowException {
    public void aVoid() throws Exception {
        throw new Exception();
    }

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run()  {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
