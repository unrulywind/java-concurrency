package com.colin.base.javaconcurrency;

/**
 * Created by Colin on 2020/6/18 10:35
 * email: colinzhaodong@gmail.com
 * desc: 星座枚举
 *
 * @author zhaod
 */
public enum ConstellationEnum {
	/**
	 * 水瓶座
	 */
	AQUARIUS(1,"水瓶座"),
	/**
	 * 双鱼座
	 */
	PISCES(2,"双鱼座"),
	/**
	 * 白羊座
	 */
	ARIES(3,"白羊座"),
	/**
	 * 金牛座
	 */
	TAURUS(4,"金牛座"),
	/**
	 * 双子座
	 */
	GEMINI(5,"双子座"),
	/**
	 * 巨蟹座
	 */
	CANCER(6,"巨蟹座"),
	/**
	 * 狮子座
	 */
	LEO(7,"狮子座"),
	/**
	 * 处女座
	 */
	VIRGO(8,"处女座"),
	/**
	 * 天秤座
	 */
	LIBRA(9,"天秤座"),
	/**
	 * 天蝎座
	 */
	SCORPIO(10,"天蝎座"),
	/**
	 * 射手座
	 */
	SAGITTARIUS(11,"射手座"),
	/**
	 * 摩羯座
	 */
	CAPRICORN(12,"摩羯座");

	/**
	 * 编码
	 */
	private final Integer code;

	/**
	 * 星座名称
	 */
	private final String name;

	ConstellationEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static String getName(Integer code){
		for (ConstellationEnum state : values()){
			if (state.getCode().equals(code)){
				return state.getName();
			}
		}
		return null;
	}
}
