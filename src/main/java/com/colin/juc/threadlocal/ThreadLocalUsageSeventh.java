package com.colin.juc.threadlocal;

/**
 * Created by Colin on 2020/4/5 15:47
 * email: colinzhaodong@gmail.com
 * desc: 演示ThreadLocal用法2：避免传递参数的麻烦
 *
 * @author zhaod
 */
public class ThreadLocalUsageSeventh {
	public static void main(String[] args) {
		new Service1().process("colin");

	}
}

class Service1 {
	public void process(String name) {
		User user = new User(name);
		UserContextHolder.holder.set(user);
		new Service2().process();
	}
}

class Service2 {

	public void process() {
		User user = UserContextHolder.holder.get();
		ThreadSafeFormatter.dateFormatThreadLocal.get();
		System.out.println("Service2拿到用户名：" + user.name);
		new Service3().process();
		System.out.println(UserContextHolder.holder.get().name);
	}
}

class Service3 {

	public void process() {
		User user = UserContextHolder.holder.get();
		System.out.println("Service3拿到用户名：" + user.name);
		UserContextHolder.holder.remove();
		User user1 = new User("张三");
		UserContextHolder.holder.set(user1);
		System.out.println(UserContextHolder.holder.get().name);
		UserContextHolder.holder.remove();
	}
}

class UserContextHolder {
	public static ThreadLocal<User> holder = new ThreadLocal<>();
}

class User {

	String name;

	public User(String name) {
		this.name = name;
	}
}
