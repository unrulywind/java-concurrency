package com.colin.base.singleton;

/**
 * Created by Colin on 2020/3/21 14:37
 * email: colinzhaodong@gmail.com
 * desc: 懒汉式（线程不安全，同步代码块，不推荐用）
 *
 * @author zhaod
 */
public class Singleton5 {
	private static Singleton5 instance;
	private Singleton5(){

	}
	public static Singleton5 getInstance(){
		if (instance == null){
			synchronized (Singleton5.class) {
				instance = new Singleton5();
			}
		}
		return instance;
	}
}
