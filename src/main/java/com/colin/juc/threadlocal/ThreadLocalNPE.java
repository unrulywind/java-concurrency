package com.colin.juc.threadlocal;

/**
 * 描述：     ThreadLocal空指针
 */
public class ThreadLocalNPE {

    ThreadLocal<Long> longThreadLocal = new ThreadLocal<Long>();

    public void set() {
        longThreadLocal.set(Thread.currentThread().getId());
    }

    public long get() {
        return longThreadLocal.get();
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadLocalNPE threadLocalNPE = new ThreadLocalNPE();
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println(threadLocalNPE.get());
                    threadLocalNPE.set();
                } catch (Exception e) {
                    System.out.println("捕捉异常");
                }
            }
        });
        thread1.start();
        thread1.join();
        int i = Runtime.getRuntime().availableProcessors();
        System.out.println("处理器:" + i);
    }
}
