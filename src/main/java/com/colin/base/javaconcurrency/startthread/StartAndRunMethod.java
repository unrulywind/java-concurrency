package com.colin.base.javaconcurrency.startthread;

/**
 * @author: BlueMelancholy
 * 2019/12/16 17:03
 * @desc
 * @email zhaod@oceansoft.com.cn
 */
public class StartAndRunMethod {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName());
        };
        runnable.run();
        new Thread(runnable).start();
    }
}
