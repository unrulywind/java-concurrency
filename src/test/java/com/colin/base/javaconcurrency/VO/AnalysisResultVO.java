package com.colin.base.javaconcurrency.VO;

/**
 * Created by Colin on 2020/6/18 15:01
 * email: colinzhaodong@gmail.com
 * desc: 身份证星座，属相，年龄，性别分析结果
 *
 * @author zhaod
 */
public class AnalysisResultVO {
	/**
	 * 星座
	 */
	private Integer constellation;

	/**
	 * 属相
	 */
	private String zodiac;

	/**
	 * 年龄
	 */
	private Integer age;

	/**
	 * 性别
	 */
	private Integer sex;

	public Integer getConstellation() {
		return constellation;
	}

	public void setConstellation(Integer constellation) {
		this.constellation = constellation;
	}

	public String getZodiac() {
		return zodiac;
	}

	public void setZodiac(String zodiac) {
		this.zodiac = zodiac;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public static AnalysisResultVOBuilder create(){
		return new AnalysisResultVOBuilder();
	}


	public static final class AnalysisResultVOBuilder {
		private Integer constellation;
		private String zodiac;
		private Integer age;
		private Integer sex;

		private AnalysisResultVOBuilder() {
		}

		public static AnalysisResultVOBuilder anAnalysisResultVO() {
			return new AnalysisResultVOBuilder();
		}

		public AnalysisResultVOBuilder constellation(Integer constellation) {
			this.constellation = constellation;
			return this;
		}

		public AnalysisResultVOBuilder zodiac(String zodiac) {
			this.zodiac = zodiac;
			return this;
		}

		public AnalysisResultVOBuilder age(Integer age) {
			this.age = age;
			return this;
		}

		public AnalysisResultVOBuilder sex(Integer sex) {
			this.sex = sex;
			return this;
		}

		public AnalysisResultVO build() {
			AnalysisResultVO analysisResultVO = new AnalysisResultVO();
			analysisResultVO.setConstellation(constellation);
			analysisResultVO.setZodiac(zodiac);
			analysisResultVO.setAge(age);
			analysisResultVO.setSex(sex);
			return analysisResultVO;
		}
	}
}
