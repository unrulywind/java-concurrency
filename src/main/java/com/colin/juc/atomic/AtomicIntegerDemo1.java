package com.colin.juc.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Colin on 2020/4/19 15:26
 * email: colinzhaodong@gmail.com
 * desc:
 *
 * @author zhaod
 */
public class AtomicIntegerDemo1 {
	private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();
	public static void atomicAdd(){
		ATOMIC_INTEGER.getAndIncrement();
	}
	private static volatile int basicCount = 0;
	public static void incrementBasic(){
		basicCount++;
	}

	public static void main(String[] args) throws InterruptedException {
		Runnable runnable = () -> {
			for (int i = 0; i < 10000; i++) {
				atomicAdd();
				incrementBasic();
			}
		};
		Thread thread = new Thread(runnable);
		Thread thread2 = new Thread(runnable);
		thread.start();
		thread2.start();
		thread.join();
		thread2.join();
		System.out.println(ATOMIC_INTEGER.get());
		System.out.println(basicCount);
	}
}
