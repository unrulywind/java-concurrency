package com.colin.juc.cache;

import com.colin.juc.cache.computable.Computable;
import com.colin.juc.cache.computable.ExpensiveFunction;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：     用装饰者模式，给计算器自动添加缓存功能
 */
public class ThirdMyCache<A, V> implements Computable<A, V> {

	private final Map<A, V> cache = new HashMap<>();

	private final Computable<A, V> c;

	public ThirdMyCache(Computable<A, V> c) {
		this.c = c;
	}

	@Override
	public V compute(A arg) throws Exception {
		System.out.println("进入缓存机制");
		System.out.println("当前的线程:" + Thread.currentThread().getName());
		V result = cache.get(arg);
		if (result == null) {
			result = c.compute(arg);
			cache.put(arg, result);
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
		ThirdMyCache<String, Integer> expensiveComputer = new ThirdMyCache<>(
				new ExpensiveFunction());
		new Thread(() -> {
			try {
				Integer result = expensiveComputer.compute("666");
				System.out.println("第一次的计算结果：" + result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		new Thread(() -> {
			try {
				Integer result = expensiveComputer.compute("666");
				System.out.println("第三次的计算结果：" + result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		new Thread(() -> {
			try {
				Integer result = expensiveComputer.compute("667");
				System.out.println("第二次的计算结果：" + result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}
}
