package com.colin.base.javaconcurrency.threadobjectclasscommonmethods;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/3/8 19:07
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class SleepInterrupted implements Runnable{
	public static void main(String[] args) throws InterruptedException {
		Thread thread = new Thread(new SleepInterrupted());
		thread.start();
		Thread.sleep(6500);
		thread.interrupt();
	}
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(new Date());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				System.out.println("我被中断了");
				e.printStackTrace();
			}
		}
	}
}
