package com.colin.base.javaconcurrency.createthreads;

/**
 * @author: BlueMelancholy
 * 2019/12/16 15:53
 * @desc 用Runnable方式创建线程
 * @email zhaod@oceansoft.com.cn
 */
public class RunnableStyle implements Runnable{
    public static void main(String[] args) {
        new Thread(new RunnableStyle()).start();
    }

    @Override
    public void run() {
        System.out.println("Runnable实现线程");
    }
}
