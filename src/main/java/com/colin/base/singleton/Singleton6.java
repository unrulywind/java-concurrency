package com.colin.base.singleton;

/**
 * Created by Colin on 2020/3/21 14:47
 * email: colinzhaodong@gmail.com
 * desc:双重检查模式
 *
 * @author zhaod
 */
public class Singleton6 {
	private volatile static Singleton6 instance;
	private Singleton6(){

	}
	public  static Singleton6 getInstance(){
		if (instance == null){
			synchronized (Singleton6.class) {
				if (instance == null) {
					instance = new Singleton6();
				}
			}
		}
		return instance;
	}
}
