package com.colin.juc.threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Colin on 2020/4/5 13:20
 * email: colinzhaodong@gmail.com
 * desc: //10个线程打印日期
 *
 * @author zhaod
 */
public class ThreadLocalUsageTwo {
	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i<30; i++) {
			int finalI = i;
			new Thread(new Runnable() {
				@Override
				public void run() {
					String date = new ThreadLocalUsageTwo().date(finalI);
					System.out.println(date);
				}
			}).start();
			TimeUnit.MILLISECONDS.sleep(100);
		}
	}

	public String date(long seconds){
		//参数的单位是毫秒，从1970.1.1 00:00:00 GMT计时
		Date date = new Date(1000 * seconds);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return simpleDateFormat.format(date);
	}
}
