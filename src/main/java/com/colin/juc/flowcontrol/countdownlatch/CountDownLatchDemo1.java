package com.colin.juc.flowcontrol.countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Colin on 2020/5/17 10:59
 * email: colinzhaodong@gmail.com
 * desc: 工厂中,质检,5个工人检查,所有人都认为通过,才通过
 *
 * @author zhaod
 */
public class CountDownLatchDemo1 {
	public static void main(String[] args) throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(5);
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 5; i++) {
			final int no = i + 1;
			Runnable runnable = () -> {
				try {
					Thread.sleep((long) (Math.random() * 10000));
					System.out.println("No." + no + "完成了检查。");
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					latch.countDown();
				}
			};
			executorService.submit(runnable);
		}
		System.out.println("等待5个人检查完.....");
		latch.await();
		System.out.println("所有人都完成了工作，进入下一个环节。");
		if (latch.getCount() == 0){
			executorService.shutdown();
			if (executorService.isShutdown()){
				System.out.println("线程池已终止");
			}
		}
	}
}
